﻿using Moq;
using NUnit.Framework;
using FileImport.File;
using Microsoft.AspNetCore.Http;
using FileImport.Model;
using System.Text;
using FileImport.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace FileImportTest
{
    [TestFixture]
    public class File
    {
        private class Resources
        {
            public HomeController Controller { get; set; }
            public Mock<IFileCSV> _file { get; set; }
            public Mock<IFormFile> _fileMock = new Mock<IFormFile>();
            public Resources(string? fileName = "", string content = "")
            {
                Controller = new HomeController();
            }
        }

        [Test]
        public void IsFileEmpty()
        {
            // Given
            string? filename = null;
            var resources = new Resources(filename);
            resources._fileMock.Setup(_ => _.FileName).Returns(filename);

            // When
            var result = resources.Controller.UploadFile(resources._fileMock.Object) as ViewResult;

            // Then
            Assert.IsNotNull(result);

            var viewModel = result.Model as ViewModel;
            Assert.IsNotNull(viewModel);
            Assert.IsFalse(viewModel?.ValidationResult.IsValid);
            Assert.AreEqual(viewModel?.ValidationResult.Messages[0].MessageText, "File is empty");
            Assert.AreEqual(viewModel?.ValidationResult.Messages[0].Code, "Empty");
        }

        [Test]
        public void FileExtension_Invalid()
        {
            // Given
            string? filename = "test.txt";
            var resources = new Resources(filename);
            resources._fileMock.Setup(_ => _.FileName).Returns(filename);
            var file = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("This is a dummy file")), 0, 0, "Data", "dummy.txt");

            // When
            var result = resources.Controller.UploadFile(resources._fileMock.Object) as ViewResult;

            // Then
            Assert.IsNotNull(result);

            var viewModel = result.Model as ViewModel;

            Assert.IsFalse(viewModel?.ValidationResult.IsValid);
            Assert.AreEqual(viewModel?.ValidationResult.Messages[0].MessageText, "Invalid File (CSV only)");
            Assert.AreEqual(viewModel?.ValidationResult.Messages[0].Code, "CSV");
        }

        [Test]
        public void FileExtension_Valid()
        {
            // Given
            string? filename = "test.txt";
            var resources = new Resources(filename);
            resources._fileMock.Setup(_ => _.FileName).Returns(filename);

            // When
            var result = resources.Controller.UploadFile(resources._fileMock.Object) as ViewResult;

            // Then
            Assert.IsNotNull(result);
            var viewModel = result?.Model as ViewModel;
            var validationResult = viewModel?.ValidationResult;
            Assert.IsNotNull(validationResult);
            Assert.IsFalse(validationResult?.IsValid);
            Assert.AreEqual(validationResult?.Messages[0].MessageText, "Invalid File (CSV only)");
            Assert.AreEqual(validationResult?.Messages[0].Code, "CSV");
        }

        [Test]
        public void File_Exists()
        {

        }


        [Test]
        public void HasElement()
        { 
            // Given
            var path = new FileInfo(@"C:\MVC Developer\FileImportTest\MockFile\file.csv");
            var memoryStream = new MemoryStream(System.IO.File.ReadAllBytes(path.FullName));
            var resources = new Resources("");
            resources._fileMock.Setup(_ => _.FileName).Returns(path.FullName);
            resources._fileMock.Setup(_ => _.Length).Returns(memoryStream.Length);
            resources._fileMock.Setup(_ => _.OpenReadStream()).Returns(memoryStream);

            // When
            var result = resources.Controller.UploadFile(resources._fileMock.Object) as ViewResult;

            Assert.IsNotNull(result);

            var viewModel = result?.Model as ViewModel;
            int? value = viewModel?.result;
            Assert.IsNotNull(value);
            Assert.True(viewModel?.ValidationResult.IsValid);
            Assert.AreEqual(value, 1);
        }

        [Test]
        public void NoElement()
        {
            // Given
            var path = new FileInfo(@"C:\MVC Developer\FileImportTest\MockFile\NoElement.csv");
            var memoryStream = new MemoryStream(System.IO.File.ReadAllBytes(path.FullName));
            var resources = new Resources("");
            resources._fileMock.Setup(_ => _.FileName).Returns(path.FullName);
            resources._fileMock.Setup(_ => _.Length).Returns(memoryStream.Length);
            resources._fileMock.Setup(_ => _.OpenReadStream()).Returns(memoryStream);

            // When
            var result = resources.Controller.UploadFile(resources._fileMock.Object) as ViewResult;

            Assert.IsNotNull(result);

            var viewModel = result?.Model as ViewModel;
            int? value = viewModel?.result;
            Assert.IsNotNull(viewModel);
            Assert.True(viewModel?.ValidationResult?.IsValid);
            Assert.AreEqual(value, -1);
        }
    }
}
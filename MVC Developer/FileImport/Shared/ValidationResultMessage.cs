﻿namespace FileImport.Shared
{
  public class ValidationResultMessage
  {
    public string? Code { get; set; }
    public string? MessageText { get; set; }
  }
}
﻿namespace FileImport.Shared
{
  public class ValidationResult
  {
    public bool IsValid { get; set; }
    public List<ValidationResultMessage> Messages { get; set; }

    public ValidationResult()
    {
      IsValid = true;
      Messages = new List<ValidationResultMessage>();
    }

    public ValidationResult Invalidate(string code, string message)
    {
      IsValid = false;
      Messages.Add(new ValidationResultMessage{Code = code, MessageText = message});

      return this;
    }
  }
}
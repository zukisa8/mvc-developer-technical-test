﻿namespace FileImport.File
{
    public class FileCSV : IFileCSV
    {
            public  int? ReadFile(IFormFile file)
            {
                int? result = null ;

                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    var fileContent = reader.ReadToEnd();
                    var fileContentArray = fileContent.Split("\r\n").Where(i => i != "").ToArray();

                    int[] elements = Array.ConvertAll(fileContentArray, s => int.Parse(s));
                    result = FindLeaderNumber(elements);
                }

                return result;
            }

            protected static int FindLeaderNumber(int[] elements)
            {
                var n = elements.Length;
                int[] l = new int[n + 1];
                l[0] = -1;

                for (int i = 0; i < n; i++)
                {
                    l[i + 1] = elements[i];
                }

                var count = 0;
                var pos = (n + 1) / 2;
                var candidate = l[pos];

                for (int i = 1; i <= n; i++)
                {
                    if (l[i] == candidate)
                        count++;
                }

                if (count > pos)
                    if (2 * count > n)
                        return candidate;
                return (-1);
            }
    }
}

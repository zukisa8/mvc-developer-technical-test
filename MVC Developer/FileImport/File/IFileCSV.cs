﻿namespace FileImport.File
{
    public interface IFileCSV
    {
        public int? ReadFile(IFormFile file);
    }
}
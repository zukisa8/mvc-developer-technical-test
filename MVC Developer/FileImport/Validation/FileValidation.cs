﻿using FileImport.Shared;
namespace FileImport.Validation
{
    public class FileValidation: IFileValidation
    {
        public ValidationResult ValidateFile (IFormFile file)
        {
            var result = new ValidationResult ();

            if (file == null || string.IsNullOrEmpty(file.FileName)) // Check if file name is empty
            {
                result.Messages.Add(new ValidationResultMessage { Code = "Empty", MessageText = "File is empty" });
                result.IsValid = false;
            }
            else if (Path.GetExtension(file.FileName) != ".csv") // Check Extension
            {
                result.Messages.Add(new ValidationResultMessage { Code = "CSV", MessageText = "Invalid File (CSV only)" });
                result.IsValid = false;
            }

            // Validate Content (only numbers)

            return result;
        }

        public bool FileExists(string path)
        {
            return !System.IO.File.Exists(path);
        }
    }
}

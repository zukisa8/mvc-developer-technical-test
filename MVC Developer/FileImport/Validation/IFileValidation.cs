﻿using FileImport.Shared;

namespace FileImport.Validation
{
    public interface IFileValidation
    {
        public ValidationResult ValidateFile(IFormFile file);
        public bool FileExists(string path);
    }
}

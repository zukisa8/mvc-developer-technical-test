﻿using FileImport.Shared;

namespace FileImport.Model
{
    public class ViewModel
    {
        public ValidationResult ValidationResult { get; set; }
        public int? result;

        public ViewModel()
        {
            ValidationResult = new ValidationResult();
            result = null;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace FileImport.Model
{
    public class FileViewModel
    {
        [Required(ErrorMessage = "Upload csv file")]
        public IFormFile? File { get; set; }

        [FileExtensions(Extensions = "")]
        public string? FileName => File?.FileName;
    }
}

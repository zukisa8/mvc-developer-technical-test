﻿using FileImport.File;
using FileImport.Model;
using FileImport.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using FileImport.Validation;

namespace FileImport.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public HomeController()
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult UploadFile(IFormFile file)
        {
            int? result = null;
            var validation = new FileValidation();
            var validationResult = validation.ValidateFile(file);

            if (validationResult.IsValid)
            {
                IFileCSV? fileCSV = new FileCSV();
                result = fileCSV.ReadFile(file);
            }


            return View("View", new ViewModel { ValidationResult = validationResult, result = result });
        }
    }
}